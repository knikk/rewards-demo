import { TypedUseSelectorHook, useSelector } from "react-redux";
import { IApplicationState } from ".";

const useTypedSelector: TypedUseSelectorHook<IApplicationState> = useSelector;

export default useTypedSelector;
