import { IMetaAction } from "..";
import {RewardsRaw} from "../../@types/RewardsRaw";
export interface IRewardsState {
  readonly result: IRewardsRaw | undefined;
}

export type ApiResponse = Record<string, any>;
export interface IRewardsRaw extends ApiResponse {
  status: number;
  data: RewardsRaw[];
}
export const RewardsActionTypes = {
  FETCH_REWARDS: "@@Study/FETCH_REWARDS",
  FETCH_REWARDS_SUCCESS: "@@Study/FETCH_ENVIRONMENT_SUCCESS",
  FETCH_REWARDS_ERROR: "@@Study/FETCH_ENVIRONMENT_ERROR",
};

export interface IDispatchToProps {
  fetchRewards1: () => IMetaAction;
}
