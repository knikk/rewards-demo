import {
  IRewardsRaw,
  IRewardsState,
  RewardsActionTypes,
} from "./RewardsTypes";
export const initialState: IRewardsState = {
  result: undefined


};
export const rewardsReducer = (
  state: IRewardsState = initialState,
  action: { type: string; payload: IRewardsRaw },
): IRewardsState => {
  switch (action.type) {
    case RewardsActionTypes.FETCH_REWARDS: {
      return { ...state };
    }
    case RewardsActionTypes.FETCH_REWARDS_SUCCESS: {
      return {
        ...state,
        result: action.payload,
      };
    }
    case RewardsActionTypes.FETCH_REWARDS_ERROR: {
      return {
        ...state,
        result: action.payload,
      };
    }
    default:
      return state;
  }
};
