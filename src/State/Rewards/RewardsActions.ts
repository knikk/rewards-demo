import { action } from "typesafe-actions";
import { IRewardsRaw, RewardsActionTypes } from "./RewardsTypes";
import { AxiosRequestConfig } from "axios";
export const fetchRewards = () =>
  action(RewardsActionTypes.FETCH_REWARDS, {},{
    method: "get" as AxiosRequestConfig["method"],
    route: "http://localhost:1337/api/rewards",
  });
export const fetchRewardsSuccess = (data: IRewardsRaw) =>
  action(RewardsActionTypes.FETCH_REWARDS_SUCCESS, data);
export const fetchRewardsError = (message: string) =>
  action(RewardsActionTypes.FETCH_REWARDS_ERROR, message);
