import { IApplicationState } from "..";

export const Rewards = (rewards: IApplicationState) => rewards.rewards;
