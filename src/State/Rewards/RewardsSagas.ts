import { call, put, takeLatest } from "redux-saga/effects";
import { fetchRewardsError, fetchRewardsSuccess,
} from "./RewardsActions";
import {RewardsActionTypes} from "./RewardsTypes";
import { IMetaAction } from "../index";
import { HttpService } from "../../ApiService/HttpService";
import {startLoader, stopLoaderOnSuccess} from "../GlobalLoader/GlobalLoaderActions";

function* handleFetch(action: IMetaAction): Generator {
  try {
    yield put(startLoader(true, ""));
    const res: any = yield call(
      HttpService,
      action.meta.method,
      action.meta.route,
      action.payload,
    );
    if (res.status === 200) {
      yield put(fetchRewardsSuccess(res));
      yield put(stopLoaderOnSuccess(false, "data fetched"));
    } else {
      yield put(stopLoaderOnSuccess(false, "something went wrong"));
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(fetchRewardsError(err.stack!));
    } else {
      yield put(fetchRewardsError("An unknown error occured"));
    }
  }
}

export default function* rewardsSaga(): Generator {
  yield takeLatest(RewardsActionTypes.FETCH_REWARDS, handleFetch);
}
