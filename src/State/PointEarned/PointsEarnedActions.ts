import { action } from "typesafe-actions";
import {EarnedPointActionTypes, IEarnedPointsRaw} from "./PointEarnedTypes";
import { AxiosRequestConfig } from "axios";
export const fetchEarnedRewards = (userId:string = "nirmal") =>
  action(EarnedPointActionTypes.FETCH_EARNED_REWARDS, {},{
    method: "get" as AxiosRequestConfig["method"],
    route: `http://localhost:1337/api/points/${userId}`,
  });
export const fetchEarnedRewardsSuccess = (data: IEarnedPointsRaw) =>
  action(EarnedPointActionTypes.FETCH_EARNED_REWARDS_SUCCESS, data);
export const fetchEarnedRewardsError = (message: string) =>
  action(EarnedPointActionTypes.FETCH_EARNED_REWARDS_ERROR, message);
