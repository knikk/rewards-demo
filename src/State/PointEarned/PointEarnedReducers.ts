import {
  EarnedPointActionTypes,
  IEarnedPointsRaw,
  IEarnedPointState,
} from "./PointEarnedTypes";
export const initialState: IEarnedPointState = {
  result: undefined


};
export const earnRewardsReducer = (
  state: IEarnedPointState = initialState,
  action: { type: string; payload: IEarnedPointsRaw },
): IEarnedPointState => {
  switch (action.type) {
    case EarnedPointActionTypes.FETCH_EARNED_REWARDS: {
      return { ...state };
    }
    case EarnedPointActionTypes.FETCH_EARNED_REWARDS_SUCCESS: {
      return {
        ...state,
        result: action.payload,
      };
    }
    case EarnedPointActionTypes.FETCH_EARNED_REWARDS_ERROR: {
      return {
        ...state,
        result: action.payload,
      };
    }
    default:
      return state;
  }
};
