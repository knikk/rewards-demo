import { IMetaAction } from "..";
import {EarnedPointRaw} from "../../@types/EarnedPointRaw";
export interface IEarnedPointState {
  readonly result: IEarnedPointsRaw | undefined;
}

export type ApiResponse = Record<string, any>;
export interface IEarnedPointsRaw extends ApiResponse {
  status: number;
  data: EarnedPointRaw[];
}
export const EarnedPointActionTypes = {
  FETCH_EARNED_REWARDS: "@@Study/FETCH_EARNED_REWARDS",
  FETCH_EARNED_REWARDS_SUCCESS: "@@Study/FETCH_EARNED_REWARDS_SUCCESS",
  FETCH_EARNED_REWARDS_ERROR: "@@Study/FETCH_EARNED_REWARDS_ERROR",
};

