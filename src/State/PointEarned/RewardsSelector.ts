import { IApplicationState } from "..";

export const EarnerRewards = (rewards: IApplicationState) => rewards.rewards;
