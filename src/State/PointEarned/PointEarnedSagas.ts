import { call, put, takeLatest } from "redux-saga/effects";
import {
  fetchEarnedRewardsError, fetchEarnedRewardsSuccess,
} from "./PointsEarnedActions";
import {EarnedPointActionTypes} from "./PointEarnedTypes";
import { IMetaAction } from "../index";
import { HttpService } from "../../ApiService/HttpService";
import {startLoader, stopLoaderOnSuccess} from "../GlobalLoader/GlobalLoaderActions";

function* handleFetch(action: IMetaAction): Generator {
  try {
    yield put(startLoader(true, ""));
    const res: any = yield call(
      HttpService,
      action.meta.method,
      action.meta.route,
      action.payload,
    );
    if (res.status === 200) {
      yield put(fetchEarnedRewardsSuccess(res));
      yield put(stopLoaderOnSuccess(false, "data fetched"));
    } else {
      yield put(stopLoaderOnSuccess(false, "something went wrong"));
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(fetchEarnedRewardsError(err.stack!));
    } else {
      yield put(fetchEarnedRewardsError("An unknown error occured"));
    }
  }
}

export default function* fetchEarnedRewardsSaga(): Generator {
  yield takeLatest(EarnedPointActionTypes.FETCH_EARNED_REWARDS, handleFetch);
}
