import { combineReducers } from "redux";
import { all, fork } from "redux-saga/effects";
import {
  Action,
  PayloadAction,
  PayloadMetaAction,
  TypeConstant,
} from "typesafe-actions";
import { History } from "history";
import { connectRouter, RouterState } from "connected-react-router";
import { rewardsReducer } from "./Rewards/RewardsReducers";
import { IRewardsState } from "./Rewards/RewardsTypes";
import rewardsSaga from "./Rewards/RewardsSagas";
import { AxiosRequestConfig } from "axios";
import {IGlobalLoaderState} from "./GlobalLoader/GlobalLoaderTypes";
import {globalLoaderReducer} from "./GlobalLoader/GlobalLoaderReducer";
import fetchEarnedRewardsSaga from "./PointEarned/PointEarnedSagas";
import updateEarnedSaga from "./UpdatePoint/UpdateEarnedSagas";
import {earnRewardsReducer} from "./PointEarned/PointEarnedReducers";
import {IEarnedPointState} from "./PointEarned/PointEarnedTypes";

export interface IApplicationState {
  rewards: IRewardsState;
  points:IEarnedPointState,
  router: RouterState;
  loader: IGlobalLoaderState;
}

export interface IMetaAction
  extends PayloadMetaAction<TypeConstant, IPayLoad, IMeta> {}
export interface IMetaActionArray
  extends PayloadMetaAction<TypeConstant, IPayLoad, IMeta[]> {}
export interface IReducerAction<TPayload>
  extends Action<TypeConstant>,
    PayloadAction<TypeConstant, TPayload> {}

type IMeta = {
  method: AxiosRequestConfig["method"];
  route: string;
  queryPayload?: {};
};
type IPayLoad = any;

export const rootReducer = (history: History) =>
  combineReducers<IApplicationState>({
    rewards: rewardsReducer,
    points: earnRewardsReducer,
    loader: globalLoaderReducer,
    router: connectRouter(history),
  });

export function* rootSaga() {
  yield all([
    fork(rewardsSaga),
    fork(fetchEarnedRewardsSaga),
    fork(updateEarnedSaga),
  ]);
}
