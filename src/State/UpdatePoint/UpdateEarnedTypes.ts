import {RewardsRaw} from "../../@types/RewardsRaw";
export interface IUpdatePointState {
  readonly result: IUpdatePointState | undefined;
}

export type ApiResponse = Record<string, any>;
export interface IUpdatePointState extends ApiResponse {
  status: number;
  data: RewardsRaw;
}
export const UpdatePointActionTypes = {
  UPDATE_EARNED_REWARDS: "@@Study/UPDATE_EARNED_REWARDS",
  UPDATE_EARNED_REWARDS_SUCCESS: "@@Study/UPDATE_EARNED_REWARDS_SUCCESS",
  UPDATE_EARNED_REWARDS_ERROR: "@@Study/UPDATE_EARNED_REWARDS_ERROR",
};

