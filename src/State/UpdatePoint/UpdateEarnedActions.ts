import { action } from "typesafe-actions";
import {UpdatePointActionTypes} from "./UpdateEarnedTypes";
import { AxiosRequestConfig } from "axios";
import {RewardsRaw} from "../../@types/RewardsRaw";
export const updateEarnedRewards = (rewards:RewardsRaw) =>
  action(UpdatePointActionTypes.UPDATE_EARNED_REWARDS, {rewards},{
    method: "post" as AxiosRequestConfig["method"],
    route: "http://localhost:1337/api/points",
  });
export const updateEarnedRewardsSuccess = (data: RewardsRaw) =>
  action(UpdatePointActionTypes.UPDATE_EARNED_REWARDS_SUCCESS, data);
export const updateEarnedRewardsError = (message: string) =>
  action(UpdatePointActionTypes.UPDATE_EARNED_REWARDS_SUCCESS, message);
