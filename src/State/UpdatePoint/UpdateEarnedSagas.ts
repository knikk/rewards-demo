import { call, put, takeLatest } from "redux-saga/effects";
import {UpdatePointActionTypes} from "./UpdateEarnedTypes";
import { HttpService } from "../../ApiService/HttpService";
import {startLoader, stopLoaderOnError, stopLoaderOnSuccess} from "../GlobalLoader/GlobalLoaderActions";
import {IMetaAction} from "../index";
import {RewardsRaw} from "../../@types/RewardsRaw";

function* update(action: IMetaAction): Generator {
  try {
    yield put(startLoader(true, ""));
    const {rewards} = action.payload;
    console.log(rewards);
    const {rating,coupon,id,...rest} = rewards;
    console.log(rest);
    const props = {...rest,userId:"admin1"}
    console.log(props);
    const res: any = yield call(
      HttpService,
      action.meta.method,
      action.meta.route, props,
    );
    if (res.status === 200) {
      yield put(stopLoaderOnSuccess(false, "data post"));
    } else {
      yield put(stopLoaderOnError(false, "something went wrong"));
    }
  } catch (err) {
    if (err instanceof Error) {
      yield put(stopLoaderOnError(false, "something went wrong"));
    } else {
      yield put(stopLoaderOnError(false,"An unknown error occurred"));
    }
  }
}

export default function* updateEarnedSaga(): Generator {
  yield takeLatest(UpdatePointActionTypes.UPDATE_EARNED_REWARDS, update);
}
