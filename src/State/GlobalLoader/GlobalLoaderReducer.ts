import {
  GlobalLoaderActionTypes,
  IGlobalLoaderRaw,
  IGlobalLoaderState,
} from "./GlobalLoaderTypes";

export const initialState: IGlobalLoaderState = {
  loading: undefined,
  message: undefined,
  error: undefined,
};

export const globalLoaderReducer = (
  state: IGlobalLoaderState = initialState,
  action: { type: string; payload: IGlobalLoaderRaw },
): IGlobalLoaderState => {
  switch (action.type) {
    case GlobalLoaderActionTypes.START_LOADER: {
      return {
        ...state,
        loading: action.payload.loading,
        message: action.payload.message,
        error: action.payload.error,
      };
    }
    case GlobalLoaderActionTypes.STOP_LOADER: {
      return {
        ...state,
        loading: action.payload.loading,
        message: action.payload.message,
      };
    }

    case GlobalLoaderActionTypes.ERROR_LOADER: {
      return {
        ...state,
        loading: action.payload.loading,
        error: action.payload.error,
      };
    }
    case GlobalLoaderActionTypes.RESET_LOADER: {
      return {
        loading: undefined,
        error: undefined,
        message: undefined,
      };
    }

    default:
      return state;
  }
};
