import { action } from "typesafe-actions";
import { GlobalLoaderActionTypes } from "./GlobalLoaderTypes";

export const startLoader = (loading: boolean, message: string | undefined) =>
  action(
    GlobalLoaderActionTypes.START_LOADER,
    { message, loading },
    {
      method: "",
      route: "",
    },
  );
export const stopLoaderOnSuccess = (
  loading: boolean,
  message: string | undefined,
) => action(GlobalLoaderActionTypes.STOP_LOADER, { message, loading });
export const stopLoaderOnError = (loading: boolean, error: string) =>
  action(GlobalLoaderActionTypes.ERROR_LOADER, { error, loading });
export const resetLoader = () =>
  action(GlobalLoaderActionTypes.RESET_LOADER, {}, {});
