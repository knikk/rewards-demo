import { IMetaAction } from "../index";

export interface IGlobalLoaderState {
  readonly loading: boolean | undefined;
  readonly message: string | undefined;
  readonly error: string | undefined;
}

export type ApiResponse = Record<string, any>;
export interface IGlobalLoaderRaw extends ApiResponse {
  loading: boolean | undefined;
  message: string | undefined;
  error: string | undefined;
}
export const GlobalLoaderActionTypes = {
  START_LOADER: "@@GLOBAL_LOADER/START_LOADER",
  STOP_LOADER: "@@GLOBAL_LOADER/STOP_LOADER",
  ERROR_LOADER: "@@GLOBAL_LOADER/ERROR_LOADER",
  RESET_LOADER: "@@GLOBAL_LOADER/RESET_LOADER",
};

export interface IDispatchToProps {
  startLoader: () => IMetaAction;
}
