import { put, StrictEffect, takeLatest } from "redux-saga/effects";
import { IMetaAction } from "../index";
import { stopLoaderOnError, stopLoaderOnSuccess } from "./GlobalLoaderActions";
import { GlobalLoaderActionTypes } from "./GlobalLoaderTypes";

function* globalLoader(
  action: IMetaAction,
): Generator<StrictEffect, void, any> {
  try {
    const {
      message,
      loading,
    }: { message: string; loading: boolean } = action.payload;
    yield put(stopLoaderOnSuccess(loading, message));
  } catch (err) {
    if (err instanceof Error) {
      yield put(stopLoaderOnError(false, err.message));
    }
    yield put(stopLoaderOnError(false, "something went wrong"));
  }
}

export default function* globalLoaderSaga(): Generator {
  yield takeLatest(GlobalLoaderActionTypes.START_LOADER, globalLoader);
}
