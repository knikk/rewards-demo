import React from "react";
import AppRoot from "./components/AppRoot";
import { Provider } from "react-redux";
import { History } from "history";
import { Store } from "redux";
import { IApplicationState } from "./State";
import { ConnectedRouter } from "connected-react-router";
import { persistStore } from "redux-persist";
import {CssBaseline, ThemeProvider} from "@material-ui/core";
import theme from "./config/theme";
interface MainProps {
    store: Store<IApplicationState>;
    history: History;
}

const App: React.FC<MainProps> = ({ store, history }) => {
    const persistor = persistStore(store);
    return (
        <Provider store={store}>
            {/*<PersistGate loading={<Loader />} persistor={persistor}>*/}
                <ThemeProvider theme={theme}>
                    <ConnectedRouter history={history}>
                        <CssBaseline />
                        <AppRoot />
                    </ConnectedRouter>
                </ThemeProvider>
            {/*</PersistGate>*/}
        </Provider>
    );
};

export default App;
