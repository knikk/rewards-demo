import { createTheme } from "@material-ui/core";

const theme = createTheme({
  mixins: {},

  overrides: {
    MuiTextField: {
      root: {
        "& label.Mui-focused": {
          color: "#000000",
        },
        "& label": {
          color: "#808080",
        },
        "& .MuiFilledInput-underline:after": {
          borderBottomColor: "#000000",
        },
        "& .MuiFilledInput-input:focus": {
          borderBottomColor: "#000000",
        },
        "& .MuiFilledInput-root": {
          backgroundColor: "#F3F3F3",
        },
        "& .MuiOutlinedInput-input": {
          paddingTop: 3,
          paddingBottom: 3,
          paddingRight: 3,
          paddingLeft: 8,
          borderBottomColor: "#000000",
        },
      },
    },

    MuiTableRow: {
      root: {
        "&:nth-child(even)": {
          "& td": {
            backgroundColor: "#ECECEC",
            zIndex: -1,
          },
        },
        "&&.MuiTableRow-hover": {
          "&:hover": {
            cursor: "pointer",
            boxShadow: "0 10px 10px 0px grey, inset 0 0 0 1px #000000",
            position: "relative",
            "& td": {
              backgroundColor: "unset",
              zIndex: 1,
            },
          },
        },
      },
    },

    MuiSelect: {
      root: {
        border: "1px solid #D3D3D3",
        borderRadius: "4px",
        backgroundColor: "#FFFFFF",
        boxShadow: "0 0 4px 0 rgba(0,0,0,0.5)",
        borderBottom: "0",
        paddingLeft: "20px",
        "& .MuiInput-underline:before": {
          borderBottom: "0",
        },
        "& .MuiInput-underline:after": {
          borderBottom: "0",
        },
      },
    },

    MuiTableCell: {
      root: {
        position: "relative",
        "&:hover": {
          border: "0",
        },
        borderBottom: "0",
      },
      body: {
        border: "0",
      },
      head: {
        border: "0",
      },
    },
    MuiTableSortLabel: {
      root: {
        color: "#808080",
        fontSize: "14px",
        letterSpacing: "0",
        lineHeight: "16px",
      },
    },

    MuiTablePagination: {
      root: {
        "& last-child": {
          border: "0",
        },
      },
    },

    MuiButtonBase: {
      root: {
        color: "#808080",
        fontSize: "14px",
        letterSpacing: "0",
        lineHeight: "16px",
      },
    },

    MuiTableHead: {
      root: {
        borderBottom: "1px solid #979797",
        borderTop: "1px solid #979797",
      },
    },

    MuiFab: {
      primary: {
        backgroundColor: "#F54022",
      },
      secondary: {
        backgroundColor: "#84D3E6",
      },
    },
  },
  palette: {
    primary: {
      main: "#808080",
    },
    secondary: {
      main: "#119BC8",
    },
    error: { main: "#B00020" },
    background: { default: "#FFFFFF" },
    text: {},
    common: {},
  },
  typography: {
    fontFamily: [
      "Roboto",
      "URW-DIN",
      "Merriweather",
      "-apple-system",
      "BlinkMacSystemFont",
      "Segoe UI",
      "Oxygen",
      "Ubuntu",
      "Cantarell",
      "Fira Sans",
      "Droid Sans",
      "Helvetica Neue",
      "sans-serif",
    ].join(","),
    h1: {
      fontSize: "96px",
      fontWeight: 300,
      letterSpacing: "-1.5px",
      lineHeight: "112px",
      color: "#808080",
    },
    h2: {
      fontSize: "60px",
      fontWeight: 300,
      letterSpacing: "-.5px",
      lineHeight: "72px",
      color: "#808080",
    },
    h3: {
      fontSize: "96px",
      lineHeight: "56px",
      color: "#808080",
      padding: "0",
    },
    h4: {
      fontSize: "34px",
      lineHeight: "36px",
      color: "#808080",
    },
    h5: {
      fontSize: "24px",
      letterSpacing: "0.18px",
      lineHeight: "24px",
      color: "#808080",
    },
    h6: {
      fontSize: "20px",
      fontWeight: 500,
      letterSpacing: "0.15px",
      lineHeight: "24px",
      color: "#808080",
    },
    subtitle1: {
      fontSize: "16px",
      fontWeight: 500,
      letterSpacing: "0.15px",
      lineHeight: "24px",
      color: "#808080",
    },
    subtitle2: {
      fontSize: "14px",
      fontWeight: 500,
      letterSpacing: "0.1px",
      lineHeight: "24px",
      color: "#808080",
    },
    body1: {
      fontSize: "16px",
      letterSpacing: ".5px",
      lineHeight: "24px",
      color: "#808080",
    },
    body2: {
      fontSize: "14px",
      fontWeight: 300,
      letterSpacing: "0.25px",
      lineHeight: "20px",
      color: "#808080",
    },
    button: {
      fontSize: "14px",
      fontWeight: 500,
      letterSpacing: "1.25px",
      lineHeight: "16px",
      color: "#808080",
    },
    caption: {
      fontSize: "12px",
      letterSpacing: "0.4px",
      lineHeight: "16px",
      color: "#808080",
    },
    overline: {
      fontSize: "10px",
      fontWeight: 500,
      letterSpacing: "1.5px",
      lineHeight: "16px",
      color: "#808080",
    },
  },
});

export default theme;
