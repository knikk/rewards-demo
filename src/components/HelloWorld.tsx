import React from "react";
import RenderForm from "./RenderForm";

const HelloWorld: React.FC<{}> = () => {
  return <div>
    Hello World
    <RenderForm/>
  </div>;
};

export default HelloWorld;
