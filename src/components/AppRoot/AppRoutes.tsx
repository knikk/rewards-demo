import React from "react";
import { IRoute } from "../Navigation/Routes";
import { Route } from "react-router-dom";

const AppRoutes: React.FC<{ routes: IRoute[]}> = ({
  routes,
}) => (
  <>
    {routes.map((route: IRoute) => {
        return (
          <Route
            exact
            path={route.path}
            key={route.path}
            component={route.component}
          />
        );
    })}

  </>
);

export default AppRoutes;
