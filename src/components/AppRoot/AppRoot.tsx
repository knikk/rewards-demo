import React from "react";
import AppRoutes from "./AppRoutes";
import {Routes} from "../Navigation";

const AppRoot: React.FC = () => {
  return (
    <React.Fragment>
      <AppRoutes routes={Routes}/>
    </React.Fragment>
  );
};

export default AppRoot;
