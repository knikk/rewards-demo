import React from "react";
import HelloWorld from "../HelloWorld";
import RenderForm from "../RenderForm";
import ListContainer from "../../Pages/Rewards/Container/ListContainer";
import EarnedPointsContainer from "../../Pages/Points/Container/ListContainer";
import {Login} from "../../Pages/Login/Login";

export interface IRoute {
  path: string;
  component: React.FC;
}


export const Routes: Array<IRoute> = [
  {
    path: "/",
    component: Login,
  },
  {
    path: "/rewards",
    component: ListContainer,
  },
  {
    path: "/points",
    component: EarnedPointsContainer,
  },
];

export default Routes;
