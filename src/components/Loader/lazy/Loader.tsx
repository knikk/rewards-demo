import { CircularProgress, styled } from "@material-ui/core";
import React from "react";
import Modal from "../../Common/Modal";

export const Loader: React.FC = () => {
  return (
    <Modal>
      <Container>
        <CircularProgress size={75} />
      </Container>
    </Modal>
  );
};

const Container = styled("div")(({ theme }) => ({
  position: "absolute",
  top: `calc(50% - 3em)`,
  left: `calc(50% - 3em)`,
}));
