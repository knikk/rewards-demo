import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { createBrowserHistory } from "history";
import configureStore from "./State/Store";
const initialState = (window as any).initialReduxState;
export const history = createBrowserHistory();
const store = configureStore(history, initialState);
ReactDOM.render(
  <React.StrictMode>
      <App store={store} history={history} />,
  </React.StrictMode>,
  document.getElementById("root")
);
