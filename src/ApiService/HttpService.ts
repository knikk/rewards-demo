import axios, { AxiosRequestConfig, AxiosResponse } from "axios";
import "./AxiosInterceptor";

export function HttpService<T>(
  method: AxiosRequestConfig["method"],
  path: string,
  data?: any,
): Promise<T[] | null> {
  const options: AxiosRequestConfig = {
    headers: {
      "Content-Type": "application/json",
    },
    method: method,
    url: path,
    data: data,
  };
  return axios(options)
    .then((response: AxiosResponse<T>) => {
      return response;
    })
    .catch((err: any) => {
      if (err.response) {
        return err.response;
      }
      return "Error";
    });
}
