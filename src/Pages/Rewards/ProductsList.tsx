import React, { useEffect } from "react";
import {IDispatchToProps, IRewardsState} from "../../State/Rewards/RewardsTypes";
import {IGlobalLoaderState} from "../../State/GlobalLoader/GlobalLoaderTypes";
import {Loader} from "../../components/Loader/lazy/Loader";
import {fetchRewards} from "../../State/Rewards/RewardsActions";
import {useDispatch} from "react-redux";
import {Coupon} from "./Card/Coupon";
import {RewardsRaw} from "../../@types/RewardsRaw";
import './ProductList.css';
import {history} from "../../index";

type AllProps = IRewardsState & IGlobalLoaderState;
export const ProductList: React.FC<AllProps> = ({
                                                result,
                                                loading,
                                            }: AllProps) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchRewards());
    }, [dispatch]);
    console.log(result);
    const showLoader = () => {
        if (loading) {
            return <Loader />;
        }
    };

    return (
        <React.Fragment>
            {showLoader()}
            {Array.isArray(result) && result.length > 0 && result[0].meal}
            <div className="container">
                <div className="header">
                  <label style={{color:"black",cursor:"pointer"}} onClick={()=>history.push('/points')}>Check My Rewards Points</label>
                </div>
                <div className="main">
                    <section className="main_body" id="main">
                        <ul>
                            {result && Array.isArray(result.data) && result.data.length > 0 && ((result.data[0]) as any).meal.map((item:RewardsRaw)=><Coupon coupons={item} key ={item.id} />)}
                        </ul>

                    </section>
                </div>
                <div className="footer">
                    developed by nirmal kumar
                </div>
            </div>
        </React.Fragment>
    );
};
