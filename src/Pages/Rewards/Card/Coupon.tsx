import React, {useCallback} from 'react';
import FavoriteIcon from '@material-ui/icons/Favorite';
import '../ProductList.css'
import {RewardsRaw} from "../../../@types/RewardsRaw";
import {useDispatch} from "react-redux";
import {updateEarnedRewards} from "../../../State/UpdatePoint/UpdateEarnedActions";

type props ={
    coupons:RewardsRaw;
}
export const Coupon = ({coupons}:props)=> {

    const dispatch = useDispatch();
    const buyNow = () => {
        console.log("aaa");
      dispatch(updateEarnedRewards(coupons));
    }
    return (
        <li className="card" >
            <img src={coupons.image} alt="Avatar"/>
            <div className="name_rating">
                <b>{coupons.provider}</b>
                <span>
                   <label>Rating :-{coupons.rating}
                   </label>
                </span>

            </div>
            <div className="city_name">{coupons.location}</div>
            <p className="coupon_description">Price: {coupons.price}</p>
            <div className="fav_mark">
                <FavoriteIcon/>
                {/*<Link to={coupons.coupon} target="_blank"  >Buy Now</Link>*/}
                <button className="buy_now" onClick={()=>buyNow()}>Buy Now</button>
            </div>
        </li>
    )
}
