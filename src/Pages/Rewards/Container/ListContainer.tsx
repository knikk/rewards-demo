import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import useTypedSelector from "../../../State/useTypedSelector";
import {IRewardsState} from "../../../State/Rewards/RewardsTypes";
import {fetchRewards} from "../../../State/Rewards/RewardsActions";
import {ProductList} from "../ProductsList";
const ListContainer = () => {
  const dispatch = useDispatch();
  const dccProps: IRewardsState = useTypedSelector(({ rewards }) => rewards);
  const globalLoaderProps = useTypedSelector(({ loader }) => loader);
  const stateToProps = {
    ...dccProps,
    ...globalLoaderProps,
  };

  return <ProductList {...stateToProps}  />;
};
export default ListContainer;
