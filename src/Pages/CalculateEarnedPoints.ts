import {IEarnedPointsRaw} from "../State/PointEarned/PointEarnedTypes";
import {IRewardsRaw} from "../State/Rewards/RewardsTypes";
import {EarnedPointRaw} from "../@types/EarnedPointRaw";

export const calculateEarnedPoints = (price:number) => {
    if (price < 50){
        return 0;
    }  if (price >= 50 && price < 100) {
        const diff = price - 50;
        return diff;
    } if (price >= 100) {
        const diff = price - 100;

        return diff * 2 + 50;
    }
}

export const totalEarnedPoints = (rows:EarnedPointRaw[]) => {
    let sum = 0;
    for (let i = 0; i< rows.length;i++){
        const tem = calculateEarnedPoints(rows[i].price);
        sum = sum + tem!;
    }
    return sum;
}