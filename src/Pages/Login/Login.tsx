import React, { useEffect } from "react";
import {history} from "../../index";
import './login.css';
export const Login: React.FC = () => {
    const [input, setInput] = React.useState('');
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput(event.target.value as string);
    };
    function handleSubmit(event:React.FormEvent) {
        event.preventDefault();
        history.push("/rewards");
       sessionStorage.setItem("user",input);
    }
    return (
        <React.Fragment>
            <div className="login">
                <h2 className="active"> Login with username </h2>

                <form autoComplete="off" onSubmit={handleSubmit}>


                    <input type="text"
                           className="text"
                           name="username"
                           value={input}
                           onChange={handleChange}/>
                        <span>username</span>

                                        <button className="signin" type={"submit"}>
                                            Sign In
                                        </button>
                </form>

            </div>
        </React.Fragment>
    );
};
