import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import useTypedSelector from "../../../State/useTypedSelector";
import {IEarnedPointState} from "../../../State/PointEarned/PointEarnedTypes";
import {EarnedPoint} from "../EarnedPoints";
const EarnedPointsContainer = () => {
  const earnedProps: IEarnedPointState = useTypedSelector(({ points }) => points);
  const globalLoaderProps = useTypedSelector(({ loader }) => loader);
  const stateToProps = {
    ...earnedProps,
    ...globalLoaderProps,
  };

  return <EarnedPoint {...stateToProps}  />;
};
export default EarnedPointsContainer;
