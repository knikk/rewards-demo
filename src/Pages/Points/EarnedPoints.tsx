import {IRewardsState} from "../../State/Rewards/RewardsTypes";
import {IGlobalLoaderState} from "../../State/GlobalLoader/GlobalLoaderTypes";
import {IEarnedPointState} from "../../State/PointEarned/PointEarnedTypes";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {fetchEarnedRewards} from "../../State/PointEarned/PointsEarnedActions";
import {Loader} from "../../components/Loader/lazy/Loader";
import React from "react";
import {makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {calculateEarnedPoints, totalEarnedPoints} from "../CalculateEarnedPoints";

type AllProps = IEarnedPointState & IGlobalLoaderState;

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});
function createData(name: string, calories: number, fat: number, carbs: number, protein: number) {
    return { name, calories, fat, carbs, protein };
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];
export const EarnedPoint: React.FC<AllProps> = ({
                                                    result,
                                                    loading,
                                                }: AllProps) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchEarnedRewards(sessionStorage.getItem('user')!));
    }, [dispatch]);
    console.log(result);
    const showLoader = () => {
        if (loading) {
            return <Loader />;
        }
    };

    return (
        <React.Fragment>
            {showLoader()}
            {result && Array.isArray(result.data) && result.data.length > 0 ?
            <TableContainer>
                <table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>UserId</TableCell>
                            <TableCell align="right">Price</TableCell>
                            <TableCell align="right">Provider&nbsp;(g)</TableCell>
                            <TableCell align="right">Location&nbsp;(g)</TableCell>
                            <TableCell align="right">Points&nbsp;(g)</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {result.data.map((row) => (
                            <TableRow key={row._id}>
                                <TableCell align={"right"}>
                                    {row.userId}
                                </TableCell>
                                <TableCell align="right">{row.price}</TableCell>
                                <TableCell align="right">{row.provider}</TableCell>
                                <TableCell align="right">{row.location}</TableCell>
                                <TableCell align="right">{calculateEarnedPoints(row.price)}</TableCell>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableCell component="th"  colSpan={4}>

                            </TableCell>
                            <TableCell component="th" align="right">Total:{totalEarnedPoints(result.data)}</TableCell>
                        </TableRow>
                    </TableBody>
                </table>
            // </TableContainer>
                : <div>No point Earned</div>}
        </React.Fragment>
    );
};
