export interface RewardsRaw {
  "id": number,
  "provider": string,
  "location": string,
  "rating": number,
  "price": number,
  "image": string
}
