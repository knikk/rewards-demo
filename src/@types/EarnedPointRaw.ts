export interface EarnedPointRaw {
  "userId": string,
  "_id"?: string,
  "provider": string,
  "location": string,
  "price": number,
  "image": string,
  "createdAt": Date,
  "updatedAt": Date,
  // "__v"?: number
}
